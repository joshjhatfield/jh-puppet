Exec {
    path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
}

## from nodes.pp
node basenode {
    stage { 'alpha': before => Stage['beta'] }
    stage { 'beta': before => Stage['gamma'] }
    stage { 'gamma': before => Stage['delta']}
    stage { 'delta': before => Stage['epsilon']}
    stage { 'epsilon': before => Stage['zeta']}
    stage { 'zeta': before => Stage['main']}
    stage { 'post': require => Stage['main'] }

    class {
    	'conftest': stage => alpha;
    }
}